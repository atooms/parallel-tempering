set term pngcairo size 640,400 enhanced font 'Verdana,10'
set out "scalability.png"
set term svg size 640,400 fname 'Verdana' fsize 11
set out "scalability.svg"

set multiplot layout 1,2
set xtics 1
set xtics (1, 2, 4, 8, 12, 18, 24)
set ytics (1, 2, 4, 8, 12, 18, 24) 
set xlabel "Number of GPUs"
set ylabel "Speedup"
set key top left Left reverse
set title 'Strong scaling (N=1200, replicas=72)' 

plot [1:][1:] 'bulk_rx_mpi_scalability_strong1200.txt' u 2:(4065.21/$3) noti 'Strong scaling (N=1200,nr=72)' w p pt 7 lc rgb "#0060ad" lw 2, x noti lc 0 dt '-'
set ylabel "Efficiency [%]"
set xtics (1, 2, 4, 6)
set ytics auto
set title 'Weak scaling (replicas/GPU=12)' 
plot [][90:]  'bulk_rx_mpi_scalability_weak150.txt' u 2:(336.65/$3*100) noti w lp pt 7 lc rgb "#0060ad" lw 2
unset multi
