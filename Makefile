PROJECT = atooms/parallel_tempering
PACKAGE = atooms.parallel_tempering

.PHONY: version test install docs coverage pep8 clean

all: install

# Version
COMMIT = $$(git describe --abbrev=6 --always 2>/dev/null || echo 0)
COMMIT_DIRTY = $$(git describe --abbrev=6 --dirty --always 2>/dev/null || echo 0)
DATE=$$(git show -s --format=%ci $(COMMIT) | cut -d ' ' -f 1)

version:
	@echo __commit__ = \'$(COMMIT_DIRTY)\' > ${PROJECT}/_commit.py
	@echo __date__ = \'$(DATE)\' >> ${PROJECT}/_commit.py

test:
	python -m unittest discover -s tests

install: version
	python setup.py install

docs:
	rm -rf docs/api
	pdoc -o docs/api --force --html --skip-errors $(PROJECT)
	emacs --batch -l ~/.emacs.d/init.el -l ~/.emacs.d/config.el --file=docs/index.org -f org-rst-export-to-rst --kill
	orgnb.py docs/*.org
	make -C docs/ singlehtml

coverage:
	coverage run --source $(PACKAGE) -m unittest discover -s tests
	coverage report

pep8:
	autopep8 -r -i $(PROJECT)
	autopep8 -r -i tests
	flake8 $(PROJECT)

clean:
	find $(PROJECT) tests -name '*.pyc' -exec rm '{}' +
	find $(PROJECT) tests -name '__pycache__' -exec rm -r '{}' +
	rm -rf build/ dist/


