#!/usr/bin/env python

import os
try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup

# Get the long description from README.md and try to convert it to
# reST. Adapted from https://bons.ai/blog/markdown-for-pypi
try:
    from pypandoc import convert
    readme = convert('README.md', 'rst')
except (ImportError, OSError):
    readme = open('README.md', 'r').read()

with open('atooms/parallel_tempering/_version.py') as f:
    exec(f.read())

setup(name='atooms-pt',
      version=__version__,
      description='Multi-CPU / multi-GPU parallel tempering',
      long_description=readme,
      author='Daniele Coslovich',
      author_email='daniele.coslovich@umontpellier.fr',
      url='https://framagit.org/atooms/parallel_tempering',
      packages=['atooms', 'atooms/parallel_tempering'],
      license='GPLv3',
      install_requires=['atooms>=1,<=3'],
      scripts=['bin/pt.py'],
      classifiers=[
          'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
          'Development Status :: 5 - Production/Stable',
          'Programming Language :: Python',
          'Programming Language :: Python :: 2',
          'Programming Language :: Python :: 2.7',
          'Intended Audience :: Science/Research',
          'Topic :: Scientific/Engineering :: Physics',
      ]
  )
