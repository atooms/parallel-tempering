


atooms-pt: Tutorial
-------------------

.. toctree::

    index

Parallel tempering is a well-established technique to accelerate simulations of complex interacting systems displaying rugged energy landscapes, such as glasses. The key idea is to perform Monte Carlo or molecular dynamics simulations of independent replicas of the system of interest at different temperatures. At regular time intervals, an exchange is attempted between different replicas and the corresponding temperatures are swapped using a Metropolis acceptance rule. Since replicas are allowed to explore states at higher temperatures, they can overcome energy barriers, thus providing a more effective and equilibrium sampling of the relevant configuration space.

The goal of this tutorial is to make a parallel tempering simulation with ``atooms-pt`` using ``LAMMPS`` as a molecular dynamics backend. We will simulate several replicas of a **binary Lennard-Jones mixture**. Each replica will be equilibrated a different temperature, in a range of thermodynamic states corresponding to normal liquid conditions.

Before we start, make sure ``atooms-pt`` and ``LAMMPS`` are both installed on your machine. The scripts below require (at least) atooms 1.7.0. The initial configuration of our simulation comes from a trajectory of a 256-particles Kob-Andersen binary mixture, available in the ``data/`` folder of the ``atooms-pt`` source code. We will do parallel tempering in temperature space using 5 different thermodynamic states. We prepare the ``LAMMPS`` backends and appropriate thermostats for each state.

.. code:: python

    import atooms.trajectory as trj
    from atooms.system import Thermostat
    from atooms.simulation import Simulation
    from atooms.backends.lammps import LAMMPS

    inp = trj.TrajectoryXYZ('../../data/kalj.xyz')
    temperatures = [1.0, 1.1, 1.2, 1.3, 1.4]
    cmd = """
    pair_style      lj/cut 2.5
    pair_coeff      1 1 1.0 1.0  2.5
    pair_coeff      1 2 1.5 0.8  2.0
    pair_coeff      2 2 0.5 0.88 2.2
    neighbor        0.3 bin
    neigh_modify    every 20 delay 0 check no
    timestep        0.002
    """

    sim = []
    for T in temperatures:
      backend = LAMMPS(inp, cmd)
      backend.system.thermostat = Thermostat(T, relaxation_time=1.0)
      backend.system.temperature = T
      # Use xyz format for output trajectory
      backend.trajectory = trj.TrajectoryXYZ
      sim.append(Simulation(backend))
    inp.close()

We can now pass the LAMMPS simulation objects to a parallel tempering instance and make 100 parallel tempering steps. Each replica of the liquid is simulated for 5000 molecular dynamics steps before an attempting an exchange between replicas. We add some observers that will write thermodynamic properties and configurations after every exchange attempt.

.. code:: python

    from atooms.parallel_tempering import ParallelTempering
    from atooms.parallel_tempering.observers import write_thermo, write_config

    pt = ParallelTempering(sim, temperatures, output_path='pt_lammps', 
                           steps=100, exchange_interval=5000,
                           checkpoint_interval=1, restart=False)
    pt.add(write_thermo, 1)
    # Only store configurations for replica with id 0
    pt.add(write_config, 1, include=[0])
    pt.run()

We are done. The output files (thermodynamic data and configurations) are in the ``pt_lammps`` folder. We can inspect the evolution of the state of each replica to make sure the replicas diffuse in temperature space. An integer id is associated to each temperature.
.. image:: pt_lammps.png
