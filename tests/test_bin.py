#!/usr/bin/env python

import os
import numpy
import glob
import unittest
import subprocess

try:
    _SKIP_RUMD = False
    import rumd
except ImportError:
    _SKIP_RUMD = True


class Test(unittest.TestCase):

    @unittest.skipIf(_SKIP_RUMD, 'missing rumd')
    def test_pt_rumd(self):
        # Somehat long test
        input_file = os.path.join(os.path.dirname(__file__), '../data/kalj.xyz.gz')
        forcefield_file = os.path.join(os.path.dirname(__file__), '../data/kalj.py')
        T = '1.0,0.95,0.9'
        cmd = 'bin/pt.py '
        opts = '-n 20 -c 1 -e 5000 -T %s --ff %s -i %s /tmp/test_pt' % (
            T, forcefield_file, input_file)
        # , stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        _ = subprocess.check_output(cmd + opts, shell=True)

        self.assertEqual(len(glob.glob('/tmp/test_pt/state/T1.0000/trajectory/*')), 21)
        self.assertEqual(len(glob.glob('/tmp/test_pt/state/T0.9500/trajectory/*')), 21)
        self.assertEqual(len(glob.glob('/tmp/test_pt/state/T0.9000/trajectory/*')), 21)
        self.assertTrue(os.path.exists('/tmp/test_pt/state/T1.0000/trajectory.thermo'))
        self.assertTrue(os.path.exists('/tmp/test_pt/state/T0.9500/trajectory.thermo'))
        self.assertTrue(os.path.exists('/tmp/test_pt/state/T0.9000/trajectory.thermo'))

        # Check that all remanining expected files are there
        for fmt in ['replica/%d.chk', 'replica/%d.out',
                    'replica/%d.out.chk', 'state/%d.out']:
            for i in range(3):
                self.assertTrue(os.path.exists('/tmp/test_pt/' + fmt % i))

        for state, ref in [(0, -1.536665),
                           (1, -1.556590),
                           (2, -1.574814)]:
            f = '/tmp/test_pt/state/%d.out' % state
            data = numpy.loadtxt(f, unpack=True)
            epot = numpy.average(data[3][1:]) / 1000
            self.assertEqual(int(data[0][-1]), 20)
            self.assertEqual(int(data[1][-1]), 20 * 5000)
            self.assertEqual(len(data[0, :]) - 1, 20)
            self.assertAlmostEqual(epot, ref, delta=0.01)

    @unittest.skipIf(_SKIP_RUMD, 'missing rumd')
    def test_pt_rumd_restart(self):
        input_file = os.path.join(os.path.dirname(__file__), '../data/kalj.xyz.gz')
        forcefield_file = os.path.join(os.path.dirname(__file__), '../data/kalj.py')
        T = '1.0,0.95,0.9'
        cmd = 'bin/pt.py '
        # First run 5 steps
        opts = '-n 5 -c 1 -e 1000 -T %s --ff %s -i %s /tmp/test_pt_restart' % (
            T, forcefield_file, input_file)
        # , stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        _ = subprocess.check_output(cmd + opts, shell=True)
        # Restart and go up to 10 steps
        opts = '-n 10 -r -c 1 -e 1000 -T %s --ff %s -i %s /tmp/test_pt_restart' % (
            T, forcefield_file, input_file)
        # , stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        _ = subprocess.check_output(cmd + opts, shell=True)

        ls = glob.glob('/tmp/test_pt_restart/state/T1.0000/trajectory/*')
        self.assertTrue(os.path.exists('/tmp/test_pt_restart/state/T1.0000/trajectory.thermo'))
        self.assertTrue(os.path.exists('/tmp/test_pt_restart/state/T0.9500/trajectory.thermo'))
        self.assertTrue(os.path.exists('/tmp/test_pt_restart/state/T0.9000/trajectory.thermo'))
        self.assertEqual(len(ls), 11)

        for state, ref in [(0, -1.5359262),
                           (1, -1.5553662),
                           (2, -1.5751214)]:
            f = '/tmp/test_pt_restart/state/%d.out' % state
            data = numpy.loadtxt(f, unpack=True)
            epot = numpy.average(data[3][1:]) / 1000
            self.assertEqual(int(data[0][-1]), 10)
            self.assertAlmostEqual(epot, ref, delta=0.02)

    # def tearDown(self):
    #     os.system('rm -rf /tmp/test_pt*')


if __name__ == '__main__':
    unittest.main()
