#!/usr/bin/env python

import os
import numpy
import glob
import unittest

from atooms.core.utils import setup_logging
from atooms.simulation import Simulation, Scheduler
from atooms.parallel_tempering import ParallelTempering
from atooms.parallel_tempering.observers import write_config, write_thermo
from atooms.parallel_tempering.exchange import npt, nvt

setup_logging(level=30)


class Test(unittest.TestCase):

    def test_pt_dry(self):
        from atooms.backends.dryrun import DryRun
        T = [1.0, 0.95, 0.9, 0.85]
        nr = len(T)
        sa = []
        for temperature in T:
            sa.append(Simulation(DryRun()))
        pt = ParallelTempering(sa, T, '/tmp/test_pt_dry', steps=10,
                               exchange_interval=10000,
                               checkpoint_interval=1, restart=False)
        pt.add(write_thermo, Scheduler(1))
        pt.run()
        data = numpy.loadtxt('/tmp/test_pt_dry/state/0.out')
        self.assertEqual(data.shape[0] - 1, 10)  # number of lines = steps
        self.assertEqual(int(data[-1][0]), 10)  # last step = steps

    def test_pt_dry_tuple(self):
        from atooms.backends.dryrun import DryRun, Thermostat
        params = [(1.0, 0.1), (0.95, 0.2), (0.9, 0.3), (0.85, 0.4)]
        sa = [Simulation(DryRun()) for _ in params]
        pt = ParallelTempering(sa, params, '/tmp/test_pt_dry', steps=10,
                               exchange_interval=10000,
                               exchange_parameters=('temperature',
                                                    'system.thermostat.mass'),
                               checkpoint_interval=1,
                               restart=False)
        pt.add(write_thermo, Scheduler(1))
        pt.run()
        data = numpy.loadtxt('/tmp/test_pt_dry/state/0.out')
        self.assertEqual(data.shape[0] - 1, 10)  # number of lines = steps
        self.assertEqual(int(data[-1][0]), 10)  # last step = steps

    def test_pt_dry_npt(self):
        from atooms.backends.dryrun import DryRun, Thermostat
        params = [1.0, 0.95, 0.90, 0.85]
        sa = [Simulation(DryRun()) for _ in params]

        class Barostat(object):
            def __init__(self, pressure=1.0):
                self.pressure = pressure
        for s in sa:
            s.system.barostat = Barostat()

        pt = ParallelTempering(sa, params, '/tmp/test_pt_dry', steps=10,
                               exchange_interval=10000,
                               exchange_parameters=('pressure', ),
                               exchange_fmt=('P%.3f'),
                               checkpoint_interval=1,
                               restart=False)
        pt.add(write_thermo, Scheduler(1))
        pt.run()
        data = numpy.loadtxt('/tmp/test_pt_dry/state/0.out')
        self.assertEqual(data.shape[0] - 1, 10)  # number of lines = steps
        self.assertEqual(int(data[-1][0]), 10)  # last step = steps

    def test_pt_dry_random(self):
        from atooms.backends.dryrun import DryRun
        T = [1.0, 0.95, 0.9, 0.85]
        nr = len(T)
        sa = []
        for temperature in T:
            sa.append(Simulation(DryRun()))
        pt = ParallelTempering(sa, T, '/tmp/test_pt_dry_random', steps=10,
                               exchange_interval=10000, exchange_scheme='random',
                               checkpoint_interval=1, restart=False)
        pt.add(write_thermo, Scheduler(1))
        pt.run()
        data = numpy.loadtxt('/tmp/test_pt_dry_random/state/0.out')
        self.assertEqual(data.shape[0] - 1, 10)  # number of lines = steps
        self.assertEqual(int(data[-1][0]), 10)  # last step = steps

    def test_pt_dry_restart(self):
        from atooms.backends.dryrun import DryRun
        T = [1.0, 0.95, 0.9, 0.85]
        pt = ParallelTempering([Simulation(DryRun()) for temp in T],
                               T, output_path='/tmp/test_pt_dry_restart', steps=2,
                               exchange_interval=10000,
                               checkpoint_interval=1, restart=False)
        pt.add(write_thermo, Scheduler(1))
        pt.run()
        pt = ParallelTempering([Simulation(DryRun()) for temp in T],
                               T, output_path='/tmp/test_pt_dry_restart', steps=5,
                               exchange_interval=10000,
                               checkpoint_interval=1, restart=True)
        pt.add(write_thermo, Scheduler(1))
        pt.run()
        data = numpy.loadtxt('/tmp/test_pt_dry_restart/state/0.out')
        self.assertEqual(data.shape[0] - 1, 5)  # number of lines = steps
        self.assertEqual(int(data[-1][0]), 5)  # last step = steps
        self.assertEqual(int(data[-1][1]), 50000)  # simulation steps
        data = numpy.loadtxt('/tmp/test_pt_dry_restart/replica/0.out')
        self.assertEqual(data.shape[0] - 1, 5)  # number of lines = steps
        self.assertEqual(int(data[-1][0]), 5)  # last step = steps
        self.assertEqual(int(data[-1][1]), 50000)  # simulation steps

    def test_pt_rumd(self):
        try:
            import rumd
        except ImportError:
            self.skipTest('missing RUMD')

        from atooms.backends.rumd import RUMD
        input_file = os.path.join(os.path.dirname(__file__), '../data/kalj.xyz.gz')
        forcefield_file = os.path.join(os.path.dirname(__file__), '../data/kalj.py')
        T = [1.0, 0.95, 0.9, 0.85]
        nr = len(T)
        from atooms.core.utils import size, rank, barrier
        for i in range(size):
            if i == rank:
                sa = []
                for temperature in T:
                    sa.append(Simulation(RUMD(input_file,
                                              forcefield_file,
                                              integrator='nvt',
                                              temperature=temperature,
                                              fixcm_interval=1000,
                                              output_path='/tmp/test_pt_rumd',
                                              dt=0.002)))
            barrier()
        pt = ParallelTempering(sa, T, '/tmp/test_pt_rumd', steps=10,
                               exchange_interval=10000,
                               checkpoint_interval=1, restart=False)
        pt.add(write_config, Scheduler(1), include=[0])
        pt.add(write_thermo, Scheduler(1))
        pt.run()
        ls = glob.glob('/tmp/test_pt_rumd/state/T1.0000/trajectory/*')
        self.assertEqual(len(ls), 11)

        for state, ref in [(0, -1.5359262),
                           (1, -1.5553662),
                           (2, -1.5751214)]:
            f = '/tmp/test_pt_rumd/state/%d.out' % state
            data = numpy.loadtxt(f, unpack=True)
            epot = numpy.average(data[3][1:]) / 1000
            self.assertEqual(int(data[0][-1]), 10)
            self.assertAlmostEqual(epot, ref, delta=0.02)

    @unittest.skip('backtab is not supported anymore')
    def test_pt_backtab(self):
        # TODO: use potably?
        import backtab
        from backtab.mc import MonteCarloBackend
        from atooms.backends.dryrun import DryRun, Thermostat
        params = [1.0, 0.95, 0.90, 0.85]
        # sa = [Simulation(MonteCarloBackend('/home/coslo/usr/atooms/backtab/data/hs_N150_phi0.5.xyz',
        #                                    '/home/coslo/usr/atooms/backtab/data/hs.json',
        #                                    temperature=T)) for T in params]
        sa = [Simulation(MonteCarloBackend('data/kalj.xyz',
                                           'data/kalj.json',
                                           temperature=T)) for T in params]
        pt = ParallelTempering(sa, params, '/tmp/test_pt', steps=10,
                               exchange_interval=1000,
                               exchange_parameters=('temperature', ),
                               exchange_fmt=('T%.3f'),
                               checkpoint_interval=0,  # 0
                               restart=False)
        pt.add(write_thermo, Scheduler(1))
        pt.run()

        for state, ref in [(0, -1.5359262),
                           (1, -1.5553662),
                           (2, -1.5751214)]:
            f = '/tmp/test_pt/state/%d.out' % state
            data = numpy.loadtxt(f, unpack=True)
            epot = numpy.average(data[3][1:]) / 1000
            self.assertEqual(int(data[0][-1]), 10)
            self.assertAlmostEqual(epot, ref, delta=0.02)

    @unittest.skip('backtab is not supported anymore')
    def test_pt_backtab_npt(self):
        # TODO: use potably?
        import backtab
        from backtab.mc import MonteCarloBackend
        from backtab.helpers import write_thermo as write_thermo_backend
        from atooms.backends.dryrun import DryRun, Thermostat
        params = [5.2, 6.0, 7.0, 8.1]
        sa = [Simulation(MonteCarloBackend('data/hs_binary_N60_phi0.5.xyz',
                                           'data/hs_binary.json',
                                           displacement_weight=1,
                                           displacement_delta=0.1,
                                           volume_weight=1./60,
                                           volume_delta=0.005,
                                           verletlist=False,
                                           temperature=1.0,
                                           pressure=P)) for P in params]
        pt = ParallelTempering(sa, params, '/tmp/test_pt', steps=10,
                               exchange_interval=500,
                               exchange=npt,
                               exchange_scheme='alternate',
                               exchange_parameters=('pressure', ),
                               exchange_fmt=('P%.1f'),
                               checkpoint_interval=1,  # 0
                               restart=False)
        pt.run()
        pt.add(write_thermo, Scheduler(1))
        pt.run(20)

        for state, Z_ref, rho_ref in [(0, 11.0, 0.490),
                                      (1, 12.0, 0.510),
                                      (2, 13.5, 0.525),
                                      (3, 15.5, 0.535)]:
            f = '/tmp/test_pt/state/%d.out' % state

            def ave(fname, col=1):
                data = numpy.loadtxt(fname, unpack=True)
                return numpy.average(data[col-1])
            rho = ave(f, col=7)
            T = 1.0
            Z = params[state][0] / (rho * T)
            phi = rho * 3.14 / 6 * (1+(1.4)**3) / 2
            self.assertAlmostEqual(Z, Z_ref, delta=0.5)
            self.assertAlmostEqual(rho, rho_ref, delta=0.01)

    def tearDown(self):
        os.system('rm -rf /tmp/test_pt*')


if __name__ == '__main__':
    unittest.main()
